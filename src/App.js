import React, { Component } from 'react';
import axios from 'axios';
import './App.css';

const Card = (props) => {
   return (
    <div className="card-margin">
      <img className="card-avatar" src={props.avatar_url} alt=""/>
      <div className="card-info">
        <div className="card-name">{props.name}</div>
        <div>{props.company}</div>
      </div>
    </div>
   ) 
}

const CardList = (props) => {
  return (
    <div>
      {props.cards.map(card => { return <Card key={card.id} {...card}/> })}
    </div>
  )
}

class Form extends Component {
  state = { userName: "" };

  handleSubmit = (event) => {
    event.preventDefault();

    axios.get(`https://api.github.com/users/${this.state.userName}`).then(resp => {
      this.props.onSubmit(resp.data);
      this.setState({ userName: "" });
    })
  }

  render() {
    return(
      <form onSubmit={this.handleSubmit}>
        <input
          value={this.state.userName}   
          onChange={(event) => this.setState({userName: event.target.value })}
          type="text" 
          placeholder="Github username"
          required/>
        <button type="submit">Add Card</button>
      </form>
    )
  };
}

class App extends Component {
  state = {
    cards: []
  }

  addNewCard = (cardInfo) => {
    this.setState(previousState => ({
      // cards: [...previousState.cards, cardInfo]
      cards: previousState.cards.concat(cardInfo)
    }))
  }

  render() {
    return (
      <div className="app">
        <Form onSubmit={this.addNewCard}/>
        <CardList cards={this.state.cards} />          
      </div>
    );
  }
}

export default App;
